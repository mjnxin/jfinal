package com.jfinal.i18n;

import com.jfinal.kit.Ret;

/**
 * 支持国际化的Ret
 * @author 郭玲 2023/6/15
 *
 */
public class I18nRet {
	public static String KEY = "message";
    private Ret ret = new Ret();
    /**
     * 禁用国际化时，使用这个原始值 或者 String.format(message, values)
     */
    private String message;
    /**
     * resourceBundle里面的key
     */
	private String i18nKey;
	/**
	 * 值参：传给messageKey的一系列参数内容
	 */
	private Object[] values;
	/**
	 * 形参：将根据paramKey从res获取的结果作为参数传入messageKey再通过res获取最终结果
	 */
	private String[] paramI18nKeys;
	
    public static I18nRet message(String message, String i18nKey) {
    	I18nRet ret = new I18nRet();
    	ret.message = message;
    	ret.i18nKey = i18nKey;
    	return ret;
    }

    public static I18nRet message(String message, String[] paramI18nKeys) {
    	I18nRet ret = new I18nRet();
    	ret.message = message;
    	ret.paramI18nKeys = paramI18nKeys;
    	return ret;
    }

    public static I18nRet message(String message, String i18nKey, Object... values) {
    	I18nRet ret = new I18nRet();
    	ret.message = message;
    	ret.i18nKey = i18nKey;
    	ret.values = values;
    	return ret;
    }

    public static I18nRet message(String message, String i18nKey, String paramI18nKey, Object... values) {
    	I18nRet ret = new I18nRet();
    	ret.message = message;
    	ret.i18nKey = i18nKey;
    	ret.paramI18nKeys = new String[] {paramI18nKey};
    	ret.values = values;
    	return ret;
    }

    public static I18nRet message(String message, String i18nKey, String... paramI18nKeys) {
    	I18nRet ret = new I18nRet();
    	ret.message = message;
    	ret.i18nKey = i18nKey;
    	ret.paramI18nKeys = paramI18nKeys;
    	return ret;
    }
    
    public static I18nRet failMessage(String message, String i18nKey) {
    	I18nRet ret = I18nRet.message(message, i18nKey);
        ret.ret.setFail();
        return ret;
    }

    public static I18nRet failMessage(String message, String[] paramI18nKeys) {
    	I18nRet ret = I18nRet.message(message, paramI18nKeys);
        ret.ret.setFail();
        return ret;
    }

    public static I18nRet failMessage(String message, String i18nKey, Object... values) {
    	I18nRet ret = I18nRet.message(message, i18nKey, values);
        ret.ret.setFail();
        return ret;
    }

    public static I18nRet failMessage(String message, String i18nKey, String... paramI18nKeys) {
    	I18nRet ret = I18nRet.message(message, i18nKey, paramI18nKeys);
        ret.ret.setFail();
        return ret;
    }

    public static I18nRet okMessage(String message, String i18nKey) {
    	I18nRet ret = I18nRet.message(message, i18nKey);
        ret.ret.setOk();
        return ret;
    }

    public static I18nRet okMessage(String message, String[] paramI18nKeys) {
    	I18nRet ret = I18nRet.message(message, paramI18nKeys);
        ret.ret.setOk();
        return ret;
    }

    public static I18nRet okMessage(String message, String i18nKey, Object... values) {
    	I18nRet ret = I18nRet.message(message, i18nKey, values);
        ret.ret.setOk();
        return ret;
    }
    
    public static I18nRet okMessage(String message, String i18nKey, String... paramI18nKeys) {
    	I18nRet ret = I18nRet.message(message, i18nKey, paramI18nKeys);
        ret.ret.setOk();
        return ret;
    }

	@SuppressWarnings("unchecked")
	public I18nRet set(Object key, Object value) {
		ret.put(key, value);
		return this;
	}
    
    public Ret toRet(String locale) {
    	return ret.set(KEY, toMessage(locale));
    }

    public String toMessage(String locale) {
    	return new ResKit(locale).message(message, i18nKey, paramI18nKeys, values);
    }
}
