package com.jfinal.i18n;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.core.JFinal;
import com.jfinal.kit.StrKit;

/**
 * Res增强类
 * @author 郭玲 2023/6/15
 *
 */
public class ResKit{
	private final String locale;
	private final Res res;

	public ResKit(String locale) {
		this.locale = locale;
		if (locale == null) {
			this.res = null;
		}
		else {
			if (!isI18nEnabled()) {
				this.res = null;
			}
			else {
				this.res = I18n.use(locale);
			}
		}
	}

	public ResKit(String baseName, String locale) {
		this.locale = locale;
		if (locale == null) {
			this.res = null;
		}
		else {
			if (!isI18nEnabled()) {
				this.res = null;
			}
			else {
				this.res = I18n.use(baseName, locale);
			}
		}
	}
	
	public static ResKit getLocaleRes(String locale) {
		return getLocaleRes(I18n.getDefaultBaseName(), locale);
	}

	public static ResKit getLocaleRes(String baseName, String locale) {
//		if ("zh-CN".equalsIgnoreCase(locale) || "zh_CN".equalsIgnoreCase(locale)) {
//			return new ResKit(baseName, "zh_CN");
//		} else if ("zh-HK".equalsIgnoreCase(locale)|| "zh-TW".equalsIgnoreCase(locale) || "zh_HK".equalsIgnoreCase(locale) || "zh-TW".equalsIgnoreCase(locale)) {
//				return new ResKit(baseName, "zh_HK");
//		} else {
//			return new ResKit(baseName, "en_US");
//		}
		String localeResName = JFinal.me().getConstants().getLocaleRes(locale);
		if (localeResName == null) {
			return new ResKit(baseName, "en_US");
		}

		return new ResKit(baseName, localeResName);
	}

	/**
	 * 取国际化语言的前端页面路径
	 *
	 * @return
	 */
	public static String getLocalePath(String locale) {
//		if ("zh-CN".equalsIgnoreCase(locale) || "zh_CN".equalsIgnoreCase(locale)) {
//			return "";
//		} else if ("zh-HK".equalsIgnoreCase(locale)|| "zh-TW".equalsIgnoreCase(locale) || "zh_HK".equalsIgnoreCase(locale) || "zh-TW".equalsIgnoreCase(locale)) {
//				return "hk";
//		} else {
//			return "en";
//		}
		String localePathName = JFinal.me().getConstants().getLocalePath(locale);
		if (localePathName == null) {
			localePathName = JFinal.me().getConstants().getLocaleRes(locale);
			if (localePathName == null) {
				return locale;
			}
		}

		return localePathName;
	}

	/**
	 * 取国际化语言的前端页面路径
	 *
	 * @return
	 */
	public static String getLocale(String locale) {
		if ("zh-CN".equalsIgnoreCase(locale) || "zh_CN".equalsIgnoreCase(locale)) {
			return "zh_CN";
		} else if ("zh-HK".equalsIgnoreCase(locale)|| "zh-TW".equalsIgnoreCase(locale) || "zh_HK".equalsIgnoreCase(locale) || "zh-TW".equalsIgnoreCase(locale)) {
				return "zh_HK";
		} else {
			return "en";
		}
	}

	private static String[] splitI18nKey(String... i18nKeys) {
		List<String> i18nKeySet = new ArrayList<>();
		for(String i18nKey : i18nKeys) {
			final String[] paramI18nKeys;
			if (i18nKey.indexOf(" ") > 0) { // 空格 分隔单词模式
				// 支持单词逗号分隔进行组词 get("开发管理平台", "develop management platform") 等效于 get("我要测试", "develop", "management", "platform")
				paramI18nKeys = i18nKey.split(" ", -1);
			}
			else if (i18nKey.indexOf(",") > 0) { // 逗号,分隔单词模式
				// 支持单词逗号分隔进行组词 get("开发管理平台", "develop,management,platform") 等效于 get("开发管理平台", "develop", "management", "platform")
				paramI18nKeys = i18nKey.split(",", -1);
			}
			else if (i18nKey.indexOf("|") > 0) { // 竖线|分隔单词模式
				// 支持单词逗号分隔进行组词 get("开发管理平台", "develop|management|platform") 等效于 get("开发管理平台", "develop", "management", "platform")
				paramI18nKeys = i18nKey.split("\\|", -1);
			}
			else if (i18nKey.indexOf(";") > 0) { // 分号;分隔单词模式
				// 支持单词逗号分隔进行组词 get("开发管理平台", "develop;management;platform") 等效于 get("开发管理平台", "develop", "management", "platform")
				paramI18nKeys = i18nKey.split(";", -1);
			}
			else {
				paramI18nKeys = new String[] {i18nKey};
			}
			
			for(String key : paramI18nKeys) {
				i18nKeySet.add(key);
			}
		}
		
		return i18nKeySet.toArray(new String[i18nKeySet.size()]);
	}
	
	public String message(String message, String i18nKey, String[] paramI18nKeys, Object... values) {
		if (isI18nEnabled()) {
			if (i18nKey != null) {
				List<Object> temp = new ArrayList<>();
				if (paramI18nKeys != null && paramI18nKeys.length > 0) {
					// 动态参数嵌套获取国际化资源
					for (String paramKey : paramI18nKeys) {
						if (paramKey == null || paramKey.length() == 0) {
							continue;
						}
						temp.add(res.get(paramKey));
					}
				}
				if (values != null && values.length > 0) {
					// 动态参数嵌套获取国际化资源
					for (Object value : values) {
						temp.add(value);
					}
				}
				if (temp.isEmpty()) {
					String val = res.get(i18nKey);
					if ("en".equals(locale)) {
						val = StrKit.firstCharToUpperCase(val);
					}
					return val;
				}	
					
				String val = res.format(i18nKey, temp.toArray());
				if ("en".equals(locale)) {
					val = StrKit.firstCharToUpperCase(val);
				}
				return val;
			}

			// 单词拼接
			if (paramI18nKeys != null && paramI18nKeys.length > 0) {
				// 支持动态参数嵌套获取国际化资源
				List<String> temp = new ArrayList<>();
				for (String paramKey : paramI18nKeys) {
					if (paramKey == null || paramKey.length() == 0) {
						continue;
					}
					temp.add(res.get(paramKey));
				}
				if (!temp.isEmpty()) {
					if ("en".equals(locale)) {
						// 第一个英文单词首字母大写, 每个单词之间空格隔开
						String val = StrKit.join(temp, " ");
						val = StrKit.firstCharToUpperCase(val);
						return val;
					}
					
					return StrKit.join(temp.toArray(new String[temp.size()]));
				}
			}
		}
		if (values == null || values.length == 0) {
			return message;
		}
		
		return String.format(message, values);
	}

	public String get(String key) {
		return res.get(key);
	}
	
	/**
	 * 国际化通用取串模式
	 *  develop=开发
	 *  management=管理
	 *  platform=平台
	 *  如果后台禁用国际化，则直接返回message
	 * @param message
	 * @param i18nKey
	 * @return
	 */
	public String get(String message, String i18nKey) {
		String[] keys = splitI18nKey(i18nKey);
		if (keys.length == 1) {
			return message(message, i18nKey, null, (Object[]) null);
		}
		return message(message, null, keys, (Object[]) null);
	}
	
	public String get1(String message, String... paramI18nKeys) {
		String[] keys = splitI18nKey(paramI18nKeys);
		return message(message, null, keys, (Object[]) null);
	}

	public String get2(String message, String i18nKey, Object... values) {
		String[] keys = splitI18nKey(i18nKey);
		if (keys.length == 1) {
			return message(message, i18nKey, null, values);
		}
		return message(message, null, keys, (Object[]) null);
	}

	public String get3(String message, String i18nKey, String... paramI18nKeys) {
		return message(message, i18nKey, paramI18nKeys, (Object[]) null);
	}

	public String get4(String message, String i18nKey, String paramI18nKey, Object... values) {
		return message(message, i18nKey, new String[] {paramI18nKey}, values);
	}
	
	public String format(String key, Object... arguments) {
		return res.format(key, arguments);
	}
	
	public Res res() {
		return res;
	}

	public boolean containsKey(String key) {
		return res.getResourceBundle().containsKey(key);
	}
	
	public static boolean isI18nEnabled() {
		return JFinal.me().getConstants().isI18nEnabled();
	}
}
